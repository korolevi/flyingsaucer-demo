<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="*:head">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
            <xsl:element name="style" namespace="{namespace-uri()}">
                <xsl:attribute name="type">text/css</xsl:attribute>
                <![CDATA[
body > table {
    width: 100% !important;
    height: 100% !important;
}
                ]]>
            </xsl:element>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>