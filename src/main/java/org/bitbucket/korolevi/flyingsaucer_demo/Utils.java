package org.bitbucket.korolevi.flyingsaucer_demo;

import org.w3c.tidy.Tidy;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Utils {
    private static final TransformerFactory TRANSFORMER_FACTORY = TransformerFactory.newInstance();
    private static final Tidy TIDY = createTidy();

    static InputStream loadResource(String resource) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
    }

    static void runXSLT(Source xml, Result result) throws TransformerException {
        Transformer transformer = TRANSFORMER_FACTORY.newTransformer();
        transformer.transform(xml, result);
    }

    static void runXSLT(Source xml, Source xslt, Result result) throws TransformerException {
        if (xslt == null) {
            Source[] xmls = tee(xml);
            xslt = TRANSFORMER_FACTORY.getAssociatedStylesheet(xmls[0], null, null, null);
            xml = xmls[1];
        }

        Transformer transformer = TRANSFORMER_FACTORY.newTransformer(xslt);
        transformer.transform(xml, result);
    }

    static void runTidy(InputStream in, OutputStream out) {
        TIDY.parse(in, out);
    }

    private static Tidy createTidy() {
        Tidy tidy = new Tidy();
        Properties tidyProps = new Properties();
        tidyProps.setProperty("new-blocklevel-tags", "canvas");
        tidy.getConfiguration().addProps(tidyProps);
        tidy.setXHTML(true);
        tidy.setInputEncoding("UTF-8");
        return tidy;
    }

    private static Source[] tee(Source xml) throws TransformerException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Transformer transformer = TRANSFORMER_FACTORY.newTransformer();
        transformer.transform(xml, new StreamResult(baos));

        Source xml1 = new StreamSource(new ByteArrayInputStream(baos.toByteArray()));
        Source xml2 = new StreamSource(new ByteArrayInputStream(baos.toByteArray()));
        return new Source[]{xml1, xml2};
    }
}
