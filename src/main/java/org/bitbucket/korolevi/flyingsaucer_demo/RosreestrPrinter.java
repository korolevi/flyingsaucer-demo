package org.bitbucket.korolevi.flyingsaucer_demo;

import com.lowagie.text.pdf.BaseFont;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Stream;

import static org.bitbucket.korolevi.flyingsaucer_demo.Utils.*;

public class RosreestrPrinter {
    private static final DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();
    private DocumentBuilder documentBuilder;
    private TransformerFactory transformerFactory = TransformerFactory.newInstance();

    public RosreestrPrinter() {
        try {
            documentBuilder = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    public void print(OutputStream pdf, List<InputStream> documents) {
        print(pdf, documents.toArray(new InputStream[documents.size()]));
    }

    public void print(OutputStream pdf, InputStream... documents) {
        try {
            ITextRenderer renderer = new ITextRenderer();
            File tmpFont = File.createTempFile("font-", ".ttf");
            tmpFont.deleteOnExit();
            Files.copy(
                    loadResource("LiberationSerif-Regular.ttf"),
                    tmpFont.toPath(),
                    StandardCopyOption.REPLACE_EXISTING
            );


            for (String fontFamily : new String[]{"Times New Roman", "times new roman"}) {
                renderer.getFontResolver().addFont(
                        tmpFont.getPath(),
                        fontFamily,
                        BaseFont.IDENTITY_H,
                        BaseFont.EMBEDDED,
                        null
                );
            }

            boolean firstDoc = true;

            for (InputStream document : documents) {
                Document doc = prepareDocument(document);
                renderer.setDocument(doc, null);
                renderer.layout();
                if (firstDoc) {
                    renderer.createPDF(pdf, false);
                    firstDoc = false;
                } else {
                    renderer.writeNextDocument();
                }
            }
            renderer.finishPDF();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Document prepareDocument(InputStream document) {
        ByteArrayInputStream bais;
        ByteArrayOutputStream baos;

        try {
            baos = new ByteArrayOutputStream();
            runXSLT(new StreamSource(document),
                    new StreamSource(loadResource("pre-processing.xsl")),
                    new StreamResult(baos));

            bais = new ByteArrayInputStream(baos.toByteArray());
            baos.reset();
            runXSLT(new StreamSource(bais), null, new StreamResult(baos));

            bais = new ByteArrayInputStream(baos.toByteArray());
            baos.reset();
            runTidy(bais, baos);

            bais = new ByteArrayInputStream(baos.toByteArray());
            baos.reset();
            runXSLT(new StreamSource(bais),
                    new StreamSource(loadResource("post-processing.xsl")),
                    new StreamResult(baos));

            Document doc = documentBuilder.newDocument();
            bais = new ByteArrayInputStream(baos.toByteArray());
            runXSLT(new StreamSource(bais), new DOMResult(doc));

            return doc;
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
    }
}
