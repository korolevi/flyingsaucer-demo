package org.bitbucket.korolevi.flyingsaucer_demo;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Main {
    public static void main(String... args) {
        List<InputStream> xmls = Stream.of(args)
                .skip(1)
                // .map(FileInputStream::new) // fuck checked exceptions
                .map(Main::openFile)
                .collect(Collectors.toList());
        OutputStream pdf;
        try {
            pdf = new FileOutputStream(args[0]);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        RosreestrPrinter printer = new RosreestrPrinter();
        printer.print(pdf, xmls.toArray(new InputStream[xmls.size()]));
    }

    private static FileInputStream openFile(String s) {
        try {
            return new FileInputStream(s);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
