package org.bitbucket.korolevi.flyingsaucer_demo;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;


import static org.bitbucket.korolevi.flyingsaucer_demo.Utils.loadResource;
import static org.junit.Assert.assertTrue;

/**
 * Created by ikorolev on 28/05/17.
 */
public class RosreestrPrinterTest {
    @Test
    public void printOnlyKp() throws Exception {
        RosreestrPrinter printer = new RosreestrPrinter();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        printer.print(baos, loadResource("kp.xml"));
        /* uncomment to generate */
        //baos.writeTo(new FileOutputStream("/tmp/kp.pdf"));

        assertTrue(TestUtils.pdfEqual(baos.toByteArray(), TestUtils.readInputStream(loadResource("kp.pdf"))));
    }

    @Test
    public void printOnlyObj() throws Exception {
        RosreestrPrinter printer = new RosreestrPrinter();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        printer.print(baos, loadResource("obj.xml"));
        /* uncomment to generate */
        //baos.writeTo(new FileOutputStream("/tmp/obj.pdf"));

        assertTrue(TestUtils.pdfEqual(baos.toByteArray(), TestUtils.readInputStream(loadResource("obj.pdf"))));
    }

    @Test
    public void printKpAndObj() throws Exception {
        RosreestrPrinter printer = new RosreestrPrinter();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        printer.print(baos, loadResource("kp.xml"), loadResource("obj.xml"));
        /* uncomment to generate */
        //baos.writeTo(new FileOutputStream("/tmp/kp-obj.pdf"));

        assertTrue(TestUtils.pdfEqual(baos.toByteArray(), TestUtils.readInputStream(loadResource("kp-obj.pdf"))));
    }

    @Test
    public void printObjAndKp() throws Exception {
        RosreestrPrinter printer = new RosreestrPrinter();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        printer.print(baos, loadResource("obj.xml"), loadResource("kp.xml"));
        /* uncomment to generate */
        //baos.writeTo(new FileOutputStream("/tmp/obj-kp.pdf"));

        assertTrue(TestUtils.pdfEqual(baos.toByteArray(), TestUtils.readInputStream(loadResource("obj-kp.pdf"))));
    }

}