package org.bitbucket.korolevi.flyingsaucer_demo;

import com.lowagie.text.pdf.PdfReader;
import org.bouncycastle.util.Arrays;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.Buffer;

/**
 * Created by ikorolev on 28/05/17.
 */
public class TestUtils {
    public static boolean pdfEqual(byte[] pdf1, byte[] pdf2) throws IOException {
        PdfReader pdfReader1 = new PdfReader(pdf1);
        PdfReader pdfReader2 = new PdfReader(pdf2);
        int numPages = pdfReader1.getNumberOfPages();
        if (pdfReader2.getNumberOfPages() != numPages) return false;
        for (int i = 0; i < numPages; i++) {
            if (!Arrays.areEqual(
                    pdfReader1.getPageContent(i),
                    pdfReader2.getPageContent(i))) {
                return false;
            }
        }
        return true;
    }

    public static byte[] readInputStream(InputStream is) throws IOException {
        int chunk = 1024*1024;
        byte[] buf = new byte[chunk];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int len;

        while ((len = is.read(buf)) != -1) {
            baos.write(buf);
        }
        return baos.toByteArray();
    }
}
